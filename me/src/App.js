import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import Deashboard from './pages/admin/Dashboard';
import Home from './pages/Home';
import Notfound from './pages/NotFound';


export default function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/deashboard' element={<Deashboard />} />                                          
          <Route path='/users' element={<Users />} />
          <Route path='/' element={<Home />} />
          <Route path='*' element={<Notfound />} />
        </Routes>
      </div>
    </Router>
  );
}


function Users() {
  return <h2>Users</h2>;
}