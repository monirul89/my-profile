import React from "react";
import { Link } from "react-router-dom";
import './user.model.css';
const Login = () => {

    return  <div className="card-area">
                <h2>Login</h2>
                <form method="POST" action="/dashboard"> 
                    <div className="form-group">
                        <label for="username">Username</label>
                        <input className="form-controls" type="text" id="username" name="username" placeholder="Username"/>
                    </div>
                    <div className="form-group">
                        <label for="password">Password</label>
                        <input className="form-controls" type="password" name="password" placeholder="Password"/>
                    </div>
                    <div className="form-group login">
                        <input type="submit" name="submit" value="Login"/>
                        <Link className="Register" to="/register"> Register </Link>
                    </div>
                </form>
            </div>
}

export default Login;