import React from "react";
import { Link } from "react-router-dom";
import '../../utilities/Loginfo.module.css';




let Signup = ()=> {
  return <div className="card-area">
            <h2>Register</h2>
            <form method="POST"> 
              <div className="form-group">
                <label for="email">Email</label>
                <input className="form-controls" type="text" id="email" name="email" placeholder="Email"/>
              </div>
              <div className="form-group">
                <label for="username">Username</label>
                <input className="form-controls" type="text" id="username" name="username" placeholder="Username"/>
              </div>
              <div className="form-group">
                <label for="password">Password</label>
                <input className="form-controls" type="password" name="password" placeholder="Password"/>
              </div>
              <div className="form-group login">
                <input type="submit" name="submit" value="Register"/>
                <Link className="login" to="/login"> Login </Link>
              </div>
            </form>
          </div>

}
export default Signup;