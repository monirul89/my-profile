import React from "react";
import { Link } from "react-router-dom";
import Banner from './Banner';

let Header = ()=>{
    return <div className="mian-content">
                <header data-aos="fade-down" className="">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <div className="logo text-left">
                            <h1>
                                <Link className="navbar-brand" to="/"><i className="fas fa-copy"></i> Monirul </Link>
                            </h1>
                        </div>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"> </span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav ml-lg-auto text-lg-right text-center">
                                <li className="nav-item active">
                                    <Link className="nav-link" to="/">Home
                                        <span className="sr-only">(current)</span>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link scroll" to="#about">About Me</Link>
                                </li>
                                <li className="nav-item dropdown">
                                    <Link className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown
                                    </Link>
                                    <div className="dropdown-menu text-lg-left text-center" aria-labelledby="navbarDropdown">
                                        <Link className="dropdown-item scroll" to="#services">Services</Link>
                                        <Link className="dropdown-item scroll" to="#skills" title="">Skills</Link>
                                        <Link className="dropdown-item scroll" to="#education" title="">My Education</Link>
                                        <Link className="dropdown-item scroll" to="#education" title="">My Experience</Link>
                                        <Link className="dropdown-item scroll" to="#clients" title="">Clients</Link>
                                        <Link className="dropdown-item scroll" to="#awards" title="">Awards</Link>
                                        <Link className="dropdown-item scroll" to="#ContactMe" title="">Contact Me</Link>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link scroll" to="#projects">Projects</Link>
                                </li>
                                <li className="nav-item" style={{'display':'flex'}}>
                                    <Link className="nav-link" to="/login">Login</Link>
                                </li>
                                <li className="nav-item" style={{'display':'flex'}}>
                                    <Link className="nav-link" to="/register">Signup</Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <div id="particles-js"></div>
                <Banner />
            </div>
}

export default Header;