import React from "react";

let Interesting = ()=>{

    return <div class="mobile-trend-agile py-5">
                <div class="container py-xl-5 py-lg-3">
                    <div class="row">
                        <div class="offset-lg-6 offset-md-5 offset-sm-3"></div>

                        <div class="col-lg-6 col-md-7 col-sm-9 what-w3lsmk text-center mt-sm-5 mt-3 pt-lg-3" data-aos="fade-down">
                            <div class="mb-lg-5 mb-4 text-center">
                                <h3 class="title-wthree text-dark mb-2">
                                    <span class="mb-2">About Me</span>Interesting </h3>
                            </div>
                            <p>I am talking about my passion- “Web Development”.</p>
                            <p>My objective is to build a career as an IT Professional in a renowned and promising organization,
                                where there is an excellent career development prospect and a challenging environment. I believe
                                hard work, strict discipline, good communication and creative problem solving skills are the
                                cornerstone of success.</p>
                            <p>I am passionately in love with Web Development and have been working from 2016 in this platform.</p>
                            <div class="button-w3ls">
                                <a href="https://gitlab.com/monirul89/monirul_cv/-/raw/main/Monirul_Islam_CV__4_.doc?inline=false" class="btn btn-sm animated-button thar-four two-buttons" download>Download <i
                                            class="fas fa-download"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
}

export default Interesting;