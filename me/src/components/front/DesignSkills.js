import React from "react";

let DesignSkills = ()=>{
    return <div className="graph py-5" id="skills">
                <div className="container pt-xl-5 pt-lg-3">
                    <div className="row">
                        <div className="col-lg-12 some-info-w3ls Design-Skills" data-aos="zoom-out-right">
                            <div className="border-styles py-2 pl-4">
                                <h3 className="title-style mb-3">Design Skills</h3>
                                <p>I can make Social icons, Images Resizing, Vector Images </p>
                            </div>
                            <div className="row skill_counter_grid">
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">100</span>%</p>
                                    <p className="para-text-w3ls text-dark">Web Design</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">100</span>%</p>
                                    <p className="para-text-w3ls text-dark">HTML,CSS</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">90</span>%</p>
                                    <p className="para-text-w3ls text-dark">Reactjs</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">70</span>%</p>
                                    <p className="para-text-w3ls text-dark">Laravel</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">40</span>%</p>
                                    <p className="para-text-w3ls text-dark">Nodejs</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">40</span>%</p>
                                    <p className="para-text-w3ls text-dark">Express</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">80</span>%</p>
                                    <p className="para-text-w3ls text-dark">Graphics</p>
                                </div>
                                <div className="col-3 w3layouts_stats_left w3_counter_grid">
                                    <p><span className="counter">80</span>%</p>
                                    <p className="para-text-w3ls text-dark">Figma</p>
                                </div>
                            </div>
                        </div>
                        {/* <div className="col-lg-7 graph-w3ls mt-lg-0 mt-5" data-aos="zoom-out-left">
                            <div className="border-styles py-2 pl-4">
                                <h3 className="title-style mb-3">Developer Skills</h3>
                            </div>
                            <div id="1chartdiv"></div>
                        </div> */}
                    </div>
                </div>
            </div>
}
export default DesignSkills;