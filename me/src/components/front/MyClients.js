import React from "react";

let MyClients = ()=>{
    return <div className="testimonials py-5" id="clients">
                <div className="container">
                    <div className="mb-sm-5 mb-4 text-center">
                        <h3 className="title-wthree text-dark mb-2">My Clients</h3>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 w3_testimonials_grids">
                            <section className="slider">
                                <div className="flexslider">
                                    <ul className="slides">

                                        <li>
                                            <a href="http://www.fusion-radiology.com/">
                                                <div className="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                                    <h4 className="text-dark text-center font-weight-light">
                                                        <i className="fas fa-quote-left mr-3"></i>Fusion is UK based Tele-radiology Company that provides general and subspecialty reporting services to support existing radiology departments within NHS and private healthcare organisations.<i className="fas fa-quote-right ml-3"></i>
                                                    </h4>
                                                    <div className="row mt-5">
                                                        <div className="col-6 test-agiles text-right">
                                                            <img src="/assets/images/te1.jpg" alt=" " className="img-fluid rounded-circle"/>
                                                        </div>
                                                        <div className="col-6 right-text-agiles text-left mt-4">
                                                            <h5 className="font-weight-bold">Fusion</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="http://www.fusion-radiology.com/">
                                                <div className="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                                    <h4 className="text-dark text-center font-weight-light">
                                                        <i className="fas fa-quote-left mr-3"></i>Allied Properties Real Estate Investment Trust REIT<i className="fas fa-quote-right ml-3"></i>
                                                    </h4>
                                                    <div className="row mt-5">
                                                        <div className="col-6 test-agiles text-right">
                                                            <img src="/assets/images/te2.jpg" alt=" " className="img-fluid rounded-circle"/>
                                                        </div>
                                                        <div className="col-6 right-text-agiles text-left mt-4">
                                                            <h5 className="font-weight-bold">Michael Doe</h5>
                                                            <p className="mt-2">Maxime Omni</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.fusion-radiology.com/">
                                                <div className="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                                    <h4 className="text-dark text-center font-weight-light">
                                                        <i className="fas fa-quote-left mr-3"></i>Premise’s platform allows you to integrate stand-alone, point solutions for a consolidated view of your data so that you and your team can make informed decisions.<i className="fas fa-quote-right ml-3"></i>
                                                    </h4>
                                                    <div className="row mt-5">
                                                        <div className="col-6 test-agiles text-right">
                                                            <img src="/assets/images/te3.jpg" alt=" " className="img-fluid rounded-circle"/>
                                                        </div>
                                                        <div className="col-6 right-text-agiles text-left mt-4">
                                                            <h5 className="font-weight-bold">Thomas Carl</h5>
                                                            <p className="mt-2">Placeat Quo</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                        <div className="col-lg-6 video-w3ls mt-lg-0 mt-sm-5 mt-4" data-aos="zoom-in-left">
                            <img src="/assets/images/testi.jpg" alt="" className="img-fluid" />
                        </div>
                    </div>
                </div>
            </div>
}

export default MyClients;