import React from "react";

let Projects = ()=>{
    return <div class="gallery py-5" id="projects">
    <div class="container">
        <div class="mb-lg-5 mb-4 text-center">
            <h3 class="title-wthree text-dark mb-2">
                <span class="mb-2">Gallery</span>My Projests</h3>
            <p class="sub-tittle-2">Below are all the projects that have been completed and gone live</p>
        </div>
        <div class="gallery-grids">
            <section>
                <ul id="da-thumbs" class="da-thumbs">
                    <li data-aos="zoom-in">
                            <a href="/assets/images/isho.jpg" class="b-link-stripe b-animate-go thickbox">
                                <img src="/assets/images/isho.jpg" alt="" class="img-fluid"/>
                            </a>
                            <div className="text-box">
                                <h5><a href="https://isho.com/">ISHO Ltd</a></h5>
                                <p><a href="https://isho.com/">Ecommerce Business</a></p>
                            </div>
                        
                    </li>
                    <li data-aos="zoom-in">
                        <a href="/assets/images/ishodesignstudio.jpg" class="b-link-stripe b-animate-go thickbox">
                            <img src="/assets/images/ishodesignstudio.jpg" alt="" class="img-fluid"/>
                        </a>
                        <div className="text-box">
                                <h5><a href="https://ishodesignstudio.com/">ISHO Design Studio</a></h5>
                                <p><a href="https://ishodesignstudio.com/">Ecommerce Business</a></p>
                            </div>
                    </li>
                    <li data-aos="zoom-in">
                      <a href="/assets/images/fimij.jpg" class="b-link-stripe b-animate-go  thickbox">
                          <img src="/assets/images/fimij.jpg" alt="" class="img-fluid"/>
                      </a>
                      <div className="text-box">
                            <h5><a href="https://me.fimij.com/">FIMIJ</a></h5>
                            <p><a href="https://me.fimij.com/">Personal Details</a></p>
                        </div>
                    </li>
                    <li data-aos="zoom-in">
                        <a href="/assets/images/premisesaas.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="/assets/images/premisesaas.jpg" alt="" class="img-fluid"/>
                        </a>
                        <div className="text-box">
                            <h5><a href="http://premisesaas.com">Premise HQ</a></h5>
                            <p><a href="http://premisesaas.com">Company Website</a></p>
                        </div>
                    </li>
                    <li data-aos="zoom-in">
                        <a href="/assets/images/themomsembroidery.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="/assets/images/themomsembroidery.jpg" alt="" class="img-fluid"/>
                        </a>
                        <div className="text-box">
                            <h5><a href="http://themomsembroidery.com/">The Mom's Embroidery</a></h5>
                            <p><a href="http://themomsembroidery.com/">Embroidery Business</a></p>
                        </div>
                    </li>
                    <li data-aos="zoom-in">
                        <a href="/assets/images/fusion-radiology.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="/assets/images/fusion-radiology.jpg" alt="" class="img-fluid"/>
                        </a>
                        <div className="text-box">
                            <h5><a href="https://www.fusion-radiology.com/">Fusion Radiology</a></h5>
                            <p><a href="https://www.fusion-radiology.com/">Fusion is a Helth reporting Business</a></p>
                        </div>
                    </li>
                                               
                    <li data-aos="zoom-in">
                        <a href="/assets/images/alliedreit.jpg" class="b-link-stripe b-animate-go  thickbox">
                            <img src="/assets/images/alliedreit.jpg" alt="" class="img-fluid"/>
                        </a>
                        <div className="text-box">
                            <h5><a href="https://www.alliedreit.com/">Allied</a></h5>
                            <p><a href="https://www.alliedreit.com/">Allied Properties Real Estate Investment Trust REIT</a></p>
                        </div>
                    </li>

                </ul>
            </section>
        </div>
    </div>
</div>
}

export default Projects;