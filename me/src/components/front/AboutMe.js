import React from "react";

let AboutMe = ()=>{
    return <section className="bottom-banner-w3layouts py-5" id="about">
                <div className="container py-xl-5 py-lg-3">
                    <div className="row py-xl-3 py-lg-3">
                        <div className="col-lg-6 feature fea-slider" data-aos="fade-right">
                            <img src="/assets/images/about.jpg" className="img-fluid" alt="" />
                        </div>
                        <div className="col-lg-6 feature pl-lg-5 mt-lg-0 mt-5 aboutMe" data-aos="fade-left">
                            <h3 className="title-wthree text-dark mb-4">About Me</h3>
                            <p>I am talking about my passion - “Web Design & Development”.</p>
                            <p>My objective is to build a career as Web Design & Development in a renowned and promising organization,
                                where there is an excellent career development prospect and a challenging environment. I believe
                                hard work, strict discipline, good communication and creative problem solving skills are the
                                cornerstone of success.</p>
                            <p>I am passionately in love with Web Development and have been working from 2017 in this platform.</p>

                            <div className="button-w3ls">
                                <a href="https://gitlab.com/monirul89/my-profile/-/raw/monir/me/public/assets/pdf/Monirul_Islam_CV.pdf?inline=false" className="btn btn-sm animated-button thar-four" download>Download <i
                                            className="fas fa-download"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
}

export default AboutMe; 