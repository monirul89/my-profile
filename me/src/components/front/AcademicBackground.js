import React from "react";

let AcademicBackground = ()=>{
    return <div className="info-w3layouts" id="education">
                <div className="w3l-overlay py-5">
                    <div className="container py-xl-5 py-lg-3">
                        <div className="edu-agile-info">
                            <div className="row">
                                <div className="col-lg-6" data-aos="fade-up"
                                    data-aos-duration="3000">
                                    <div className="grid-flex bg-white p-5">
                                        <div className="pl-4 title-edu py-2 mb-5">
                                            <h4 className="aboutbtm-head"><i className="fas fa-book-reader mb-2"></i><span>Academic Background</span></h4>
                                        </div>
                                        <div className="grids-agiles-one">
                                            <h5 className="text-dark">BSc in Computer Science & Engineering</h5>
                                            <p>Dhaka International University</p>
                                            <p>CGPA - 3.42 (Out of 4) </p>
                                        </div>
                                        <div className="grids-agiles-one my-3">
                                            <h5 className="text-dark">Diploma in Computer Science & Engineering</h5>
                                            <p>Bogra Polytechnic Institute, Bogra</p>
                                            <p>CGPA - 3.09 (Out of 4) </p>
                                        </div>
                                        <div className="grids-agiles-one my-3">
                                            <h5 className="text-dark">Secondary School Certificate (Dakhil)</h5>
                                            <p>Bannakandi Dakhil Madrasha, Madrasah Board, Dhaka</p>
                                            <p>CGPA - 4.42 (Out of 5) </p>
                                        </div>
                                        <div className="style-book-wthree">
                                            <i className="fas fa-book-open"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 mt-lg-0 mt-5" data-aos="fade-up"
                                    data-aos-duration="3000">
                                    <div className="grid-flex bg-white p-5">
                                        <div className="pl-4 title-edu py-2 mb-5">
                                            <h4 className="aboutbtm-head"><i className="fas fa-chalkboard-teacher mb-2"></i> <span>Experience</span></h4>
                                        </div>
                                        <div className="grids-agiles-one">
                                            <h5 className="text-dark">Web Designer & Developer </h5>
                                            <p>ISHO Ltd<br />March 1, 2020 - Continuing</p>
                                        </div>
                                        <div className="grids-agiles-one">
                                            <h5 className="text-dark">Web Developer</h5>
                                            <p>Codemen Solution Inc.<br/> April 1, 2018 - Feb 28, 2020</p>
                                        </div>

                                        <div className="grids-agiles-one">
                                            <h5 className="text-dark">Course Web App Development PHP</h5>
                                            <p>BASIS Institute of Technology <br /> 2017-2018</p>
                                        </div>
                                        <div className="grids-agiles-one">
                                            <h5 className="text-dark mb-2">Course Web Development</h5>
                                            <p>E-Future Bangladesh Ltd<br/>2013-2014</p>
                                        </div>
                                        <div className="style-book-wthree">
                                            <i className="fas fa-edit"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
}

export default AcademicBackground;