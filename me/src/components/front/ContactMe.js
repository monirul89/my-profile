import React from "react";

let ContactMe = ()=>{
    return <section>
                <div className="Contact">
                    <div className="container py-xl-5 py-lg-3">
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-6 w3l-footer-logo">
                                <div className="contact_box contact-address">
                                    <h2>Contact Information</h2>
                                    <p><strong>Name : </strong>Md Monirul Islam</p>
                                    <p><strong>Profition : </strong>Web Designer</p>
                                    <p><strong>Gitlab : </strong>https://gitlab.com/monirul89</p>
                                    <p><strong>Email : </strong>monirul89@gmail.com</p>
                                    <p><strong>Phone : </strong>01722795884</p>
                                    <p><strong>Home : </strong>01912191584</p>
                                    <p><strong>Address : </strong>Rampura, Dhaka-1219</p>
                                </div>
                            </div>
                            <div className="col-lg-8 col-md-8 col-sm-6  w3l-footer text-lg-right text-center mt-3">
                                <div className="contact_box contact-mail">
                                    <form method="POST" action="">
                                        <h2>Contact Me</h2>
                                        <div className="info_boxs">
                                            <div className="formgroup">
                                                <label for="client_name">Name</label>
                                                <input type="text" id="client_name" name="client_name" className="control-from" placeholder="Name"/>
                                            </div>
                                            
                                            <div className="formgroup">
                                                <label for="client_email">Email ID</label>
                                                <input type="email" id="client_email" name="client_email" className="control-from" placeholder="Name"/>
                                            </div>
                                            <div className="formgroup">
                                                <label for="client_phone">Phone No</label>
                                                <input type="text" id="client_phone" className="control-from" placeholder="Name"/>
                                            </div>
                                            <div className="formgroup">
                                                <label for="client_address">Address</label>
                                                <textarea id="client_address" className="control-from" placeholder="Address"> </textarea>
                                            </div>
                                            <div className="formgroup">
                                                <label for="client_message">Message</label>
                                                <textarea id="client_message" className="control-from" placeholder="Message"> </textarea>
                                            </div>
                                            <div className="formgroup">
                                                <label for="cname"></label>
                                                <input type="submit" id="submit" name="submit" className="btn button"/>
                                            </div>
                                        </div>  
                                     </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
}


export default ContactMe;