import React from "react";

let MyServices = ()=>{
    return <div className="why-choose-agile py-5" id="services">
                <div className="container py-xl-5 py-lg-3">
                    <div className="mb-lg-5 mb-4 text-center">
                        <h3 className="title-wthree text-white mb-2">
                            <span className="text-dark mb-2">My Works</span>My Services</h3>
                    </div>
                    <div className="row agileits-w3layouts-grid my-lg-4">                        
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-asymmetrik"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Reactjs</h4>
                                    <p>Image, PDF, PSD, AI, Figma Layout to Convert Reactjs</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-xbox"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Basic Nodejs</h4>
                                    <p>I know the basics of Nodejs</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-blackberry"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Basic Express</h4>
                                    <p>I know the basics of Express</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row agileits-w3layouts-grid my-lg-4">
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fas fa-spinner"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Laravel Framwork</h4>
                                    <p>The PHP scripting language has a variety of frameworks. Clearly Laravel is the best framework in 2018.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-xbox"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Development</h4>
                                    <p>I can make wordpress theme development from scratch. </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-forumbee"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Figma</h4>
                                    <p>Create Social Icon, Image manipulations, adjustments, Resizing, Images Optimization etc</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="row agileits-w3layouts-grid">
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fas fa-spinner"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Wordpress</h4>
                                    <p>I can make wordpress theme development from scratch.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-audible"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Layout Convert</h4>
                                    <p>PSD to HTML, CSS, Reactjs, Foigma Layout to HTML Layout</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fas fa-gamepad"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Bootstrap 5</h4>
                                    <p>Doing Web development using Bootstrap, It's Most Propular CSS Framwork</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row agileits-w3layouts-grid pt-md-4">
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-blackberry"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Customization</h4>
                                    <p>I can make wordpress theme Customization and debug.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-affiliatetheme"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9 agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Adobe Photoshop</h4>
                                    <p>Create Social Icon, Image manipulations, adjustments, Resizing, Images Optimization etc.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                            <div className="row wthree_agile_us py-md-4 py-3">
                                <div className="col-lg-3 col-sm-2 col-3 pr-lg-2">
                                    <div className="wthree_features_grid text-center py-3">
                                        <i className="fab fa-accusoft"></i>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-10 col-9  agile-why-text">
                                    <h4 className="text-white font-weight-bold mb-3">Illustrator</h4>
                                    <p>Create Social Icon, Image manipulations, adjustments, Resizing, Images Optimization etc</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
}

export default MyServices;