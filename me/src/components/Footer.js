
import React from "react";

let Footer = ()=>{
    return  <footer>
                <div className="w3ls-footer-grids py-4">
                    <div className="container py-xl-5 py-lg-3">
                        <div className="row">
                            <div className="col-lg-5 w3l-footer-logo">
                                {/* footer logo  */}
                                <a className="navbar-brand" href="/"><i className="fas fa-copy"></i> Monirul</a>                                
                            </div>
                            <div className="col-lg-5 w3l-footer text-lg-right text-center mt-3">
                                <ul className="list-unstyled footer-nav-wthree">
                                    <li className="mr-4">
                                        <a href="/" className="active">Home</a>
                                    </li>
                                    <li className="mr-4">
                                        <a className="scroll" href="#about">About Me</a>
                                    </li>
                                    <li className="mr-4">
                                        <a className="scroll" href="#services">My Services</a>
                                    </li>
                                    <li>
                                        <a className=" scroll" href="#projects">Projects</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-2 mt-lg-0 mt-3">
                                <div className="button-w3ls mt-0 text-lg-right text-center">
                                    <a href="mailto:monirul89@gmail.com" className="btn btn-sm animated-button thar-four">Hire Me</a>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top mt-4 pt-lg-4 pt-3 text-lg-left text-center">
                            <p className="col-lg-8 copy-right-grids mt-lg-1">© right 2022 by <a href="mailto:monirul89@gmail.com"> Monirul Islam </a>
                            </p>
                            {/* social icons  */}
                            <div className="col-lg-4 w3social-icons text-lg-right text-center mt-lg-0 mt-3">
                                <ul>
                                    <li>
                                        <a href="https://www.facebook.com/monirul89" target="_blank" className="rounded-circle" rel="noreferrer">
                                            <i className="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li className="px-2">
                                        <a href="https://www.pinterest.com/monirul89/_saved/" className="rounded-circle">
                                            <i className="fab fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/csemonirul" target="_blank" className="rounded-circle" rel="noreferrer">
                                            <i className="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/monirul892020/" target="_blank" className="rounded-circle" rel="noreferrer">
                                            <i className="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    {/* <li className="pl-2">
                                        <a href="#" className="rounded-circle">
                                            <i className="fab fa-dribbble"></i>
                                        </a>
                                    </li> */}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
}

export default Footer;