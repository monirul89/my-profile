import React from "react";

let Banner = ()=>{
    return  <div className="banner-text">
                <div className="effect-text-w3ls">
                    <div className="container">
                        <h2>Professional Web Designer & Developer</h2>
                        <span className="mytext1 uppercase mt-2">Hello, This is </span>
                        <span className="mytext2"> Monirul Islam </span>
                        <p className="para-style text-light mt-xl-4 mt-3">I am talking about my passion.</p>
                        <div className="button-w3ls aos-init aos-animate" data-aos="fade-up">
                            <a href="https://gitlab.com/monirul89/my-profile/-/raw/monir/me/public/assets/pdf/Monirul_Islam_CV.pdf?inline=false" className="btn btn-sm animated-button thar-three mr-2" download>Download 
                            <i className="fas fa-download"></i></a>
                            <a href="mailto:monirul89@gmail.com" className="btn btn-sm animated-button thar-four">Hire Me</a>
                        </div>
                    </div>
                </div>
            </div>
}

export default Banner;