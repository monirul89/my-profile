import React from "react";

let Notfound = ()=> {
  return <section className="section-notfound"> 
    <h2>Not found</h2>
  </section>

}
export default Notfound;