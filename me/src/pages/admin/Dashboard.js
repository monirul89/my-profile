import { Component } from 'react';
import axios from "axios";
class Deashboard extends Component {

  state = {
    surahList: []
    }

    componentDidMount(){
      this.fetchSurahList();
    }
    fetchSurahList = () => {
      axios.get("http://api.alquran.cloud/v1/surah").then( res => res.data ).then((data)=>{
        console.log(data.data);
        this.setState({
          surahList:data.data
        });
      })
      // fetch('http://api.alquran.cloud/v1/surah').then( res => res.json() ).then((data)=>{
      //   console.log(data);
      //   this.setState({
      //     surahList: data.data
      //   });
      // })
    }

  render(){
    return <div>{this.state.surahList.map((item) => <p>{item.englishNameTranslation}</p>)}</div>
  }
}

export default  Deashboard;