import React from "react";
import Footer from '../components/Footer';
import AboutMe from '../components/front/AboutMe';
import AcademicBackground from '../components/front/AcademicBackground';
import DesignSkills from '../components/front/DesignSkills';
import MyClients from '../components/front/MyClients';
import MyServices from '../components/front/MyServices';
// import Interesting from '../components/front/Interesting';
import Projects from '../components/front/Projects';
import Header from '../components/Header';
import ContactMe from '../components/front/ContactMe';

let Home = ()=> {

  return <div className="section-area"> 
            <Header />
            <AboutMe />
            <MyServices />
            <DesignSkills />
            <AcademicBackground />
            <MyClients />
            {/* <Interesting /> */}
            <Projects />
            <ContactMe />
            <Footer />
    </div>

}

export default Home;