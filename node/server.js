const express = require('express');
const mysql = require('mysql');
const app = express();
const port = 5000


app.get('/', (req, res) => {
    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ dataInsert(con); } });
    function dataInsert(con){
        var sql = "SELECT * FROM `school_list`";
        con.query(sql, function (err, result) {
            if (err) throw err;
            var JSONData = JSON.stringify(result);
            res.end(JSONData);
        });
    }
});

app.post('/', (req, res) => {
    let name = req.query.name;
    let roll = req.query.roll;
    let no_class = req.query.class;
    let phone = req.query.phone;
    let city = req.query.city;

    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ dataInsert(con); } });
    function dataInsert(con){
        var sql = "INSERT INTO `school_list`(`name`, `roll`, `class`, `phone`, `city`) VALUES(?, ?, ?, ?, ?)";
        con.query(sql, [name,roll,no_class,phone,city], function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });
    }
});
app.put('/', (req, res) => {

    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ dataInsert(con); } });
    function dataInsert(con){
        var sql = "UPDATE INTO `school_list`(`name`, `roll`, `class`, `phone`, `city`) VALUES(?, ?, ?, ?, ?)";
        con.query(sql, [name,roll,no_class,phone,city], function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });
    }
});

app.delete('/', (req, res) => {
    let id = req.query.id;
    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ deleteData(con); } });
    function deleteData(con){
        var sql = "DELETE FROM `school_list` WHERE id=?";
        con.query(sql, [id], function (err, result) {
            if (err) throw err;
            console.log("1 record deleted");
        });
    }

});

app.get('/users', (req, res) => {
    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ dataInsert(con); } });
    function dataInsert(con){
        var sql = "SELECT * FROM `users`";
        con.query(sql, function (err, result) {
            if (err) throw err;
            var JSONData = JSON.stringify(result);
            res.end(JSONData);
        });
    }
});

app.post('/users', (req, res) => {

    let user_name = req.query.user_name;
    let email = req.query.email;
    let password = req.query.password;

    let con = mysql.createConnection({ host: 'localhost', user: 'root',  password: '',  database: 'school'});
    con.connect(function(error){ if(!error){ dataInsert(con); } });
    function dataInsert(con){
        var sql = "INSERT INTO `users`(`user_name`, `email`, `password`) VALUES( ?, ?, ? )";
        con.query(sql, [user_name, email, password], function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });
    }
});

app.listen(port, () => {
  console.log(`Listening Url Port: http://localhost:${port}`)
})